FROM debian:unstable

RUN echo "deb http://deb.debian.org/debian/ unstable main contrib non-free" > /etc/apt/sources.list \
 && dpkg --add-architecture i386 \
 && apt-get update \
 && yes 2 | DEBIAN_FRONTEND=teletype apt-get install -y steam mesa-vulkan-drivers \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends locales libegl1:i386 \
 && rm -rf /var/lib/apt/lists/*

COPY group passwd shadow /etc/
COPY keyboard /etc/default

RUN chmod 640 /etc/shadow \
 && chmod u+s /usr/bin/bwrap \
 && sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
 && locale-gen

USER will
ENV SHELL=/bin/bash
WORKDIR /home/will

